﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppliTP
{
    public partial class FormAjout : Form
    {
        public FormAjout()
        {
            InitializeComponent();
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (tbAjout.Text != "")
            {
                if (Outils.verifDoublon(tbAjout.Text) == true)
                {
                    Outils.AjouterUtilisateur(tbAjout.Text);
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Ce prénom existe déja");
                }
                tbAjout.Text = "";
                
            }
            else
            {
                MessageBox.Show("tu dois entre un prénom");
            }
        }
        //Empeche l'utilisateur de rentrer des , dans son prenom(lecture du fichier)
        private void tbAjout_TextChanged(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar==',')
                e.Handled = true; 
        }
    }
}
