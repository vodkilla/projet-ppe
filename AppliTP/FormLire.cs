﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppliTP
{
    public partial class FormLire : Form
    {

        Random genRb = new Random();
        string image;
        int rbSolution;
        public FormLire()
        {
            InitializeComponent();
            
        }
        private void FormLire_Load(object sender, EventArgs e)
        {
            //On affiche le score dans un label
            lbScore.Text = Convert.ToString(Outils.GetScoreLire());
            Outils.SetNomImage();
            ////On recupere le nom de l'image
            image = Outils.GetNomImage();
            pictureBox1.Image = imageList1.Images[image];
            //radio bouton de solution initialisé a 2
            rbSolution = 2;
            rbChoix2.Text = Outils.GetNomImage();
            rbChoix3.Text = Outils.GenTexteRbDiff1(image);
            rbChoix1.Text = Outils.GenTexteRbDiff2(image, rbChoix3.Text);
        }
        private void btValider_Click(object sender, EventArgs e)
        {
            // Efface le contenu des textbox
            lbAttention.Text = "";
            lbReponse.Text = "";
            // Verifie que le radio bouton coché correspond au bouton de solution
            // Si c'est le cas on incrémente le score
            if (rbChoix1.Checked == true)
            {
                if (rbSolution == 1)
                {
                    Outils.IncrementerScoreLire();
                    Outils.SetFichier();
                }
                else
                {
                    lbReponse.Text= ("L'image était: "+image+".");
                    lbChoix.Text = ("Ta réponse: " + rbChoix1.Text+".");
                }
            }
            else
                if (rbChoix2.Checked == true)
                {
                    if (rbSolution == 2)
                    {
                        Outils.IncrementerScoreLire();
                        Outils.SetFichier();
                    }
                    else
                    {
                        lbReponse.Text = ("L'image était: " + image + ".");
                        lbChoix.Text = ("Ta réponse: " + rbChoix2.Text + ".");
                    }
                }
                else
                    if (rbChoix3.Checked == true)
                    {
                        if (rbSolution == 3)
                        {
                            Outils.IncrementerScoreLire();
                            Outils.SetFichier();
                        }
                        else
                        {
                            lbReponse.Text = ("L'image était: " + image + ".");
                            lbChoix.Text = ("Ta réponse: " + rbChoix3.Text + ".");
                        }
                    }
                    else
                    {
                        lbAttention.Text = ("Il faut cocher un bouton !");
                        lbReponse.Text = ("L'image était: " + image + " .");
                    }
            lbScore.Text = Convert.ToString(Outils.GetScoreLire());
            //On choisit la prochaine image
            Outils.SetNomImage();
            //On recupere son nom
            image = Outils.GetNomImage();
            //On genere un nombre aleatoire entre 1 et 3 correspondant au bouton de solution
            rbSolution = genRb.Next(1, 4);
            // On affiche l'image
            pictureBox1.Image = imageList1.Images[image];

            // On affiche le texte des prochain radio bouton
            if (rbSolution == 1)
            {
                rbChoix1.Text = Outils.GetNomImage();
                rbChoix2.Text = Outils.GenTexteRbDiff1(image);
                rbChoix3.Text = Outils.GenTexteRbDiff2(image, rbChoix2.Text);
            }
            else
                if(rbSolution == 2)
                {
                    rbChoix2.Text = Outils.GetNomImage();
                    rbChoix1.Text = Outils.GenTexteRbDiff1(image);
                    rbChoix3.Text = Outils.GenTexteRbDiff2(image, rbChoix1.Text);
                }
                else
                    if (rbSolution == 3)
                    {
                        rbChoix3.Text = Outils.GetNomImage();
                        rbChoix1.Text = Outils.GenTexteRbDiff1(image);
                        rbChoix2.Text = Outils.GenTexteRbDiff2(image, rbChoix1.Text);
                    }
            // On décoche les radio bouton          
            rbChoix1.Checked = false;
            rbChoix2.Checked = false;
            rbChoix3.Checked = false;
         } 
    }
}
