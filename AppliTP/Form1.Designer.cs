﻿namespace AppliTP
{
    partial class Form1
    {
        // Déclaration des fenêtre
        FormMenu fMenu;
        FormAjout fAjout;
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btAjouter = new System.Windows.Forms.Button();
            this.btValider = new System.Windows.Forms.Button();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.tbJoueurScore = new System.Windows.Forms.TextBox();
            this.btQuitter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(412, 531);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(165, 123);
            this.btAjouter.TabIndex = 7;
            this.btAjouter.Text = "Ajouter";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // btValider
            // 
            this.btValider.Location = new System.Drawing.Point(89, 522);
            this.btValider.Name = "btValider";
            this.btValider.Size = new System.Drawing.Size(127, 106);
            this.btValider.TabIndex = 6;
            this.btValider.Text = "VAlider";
            this.btValider.UseVisualStyleBackColor = true;
            this.btValider.Click += new System.EventHandler(this.btValider_Click);
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(129, 113);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(152, 31);
            this.tbLogin.TabIndex = 4;
            // 
            // tbJoueurScore
            // 
            this.tbJoueurScore.Enabled = false;
            this.tbJoueurScore.Location = new System.Drawing.Point(53, 238);
            this.tbJoueurScore.Multiline = true;
            this.tbJoueurScore.Name = "tbJoueurScore";
            this.tbJoueurScore.Size = new System.Drawing.Size(877, 269);
            this.tbJoueurScore.TabIndex = 8;
            // 
            // btQuitter
            // 
            this.btQuitter.Location = new System.Drawing.Point(730, 531);
            this.btQuitter.Name = "btQuitter";
            this.btQuitter.Size = new System.Drawing.Size(165, 123);
            this.btQuitter.TabIndex = 9;
            this.btQuitter.Text = "Quitter";
            this.btQuitter.UseVisualStyleBackColor = true;
            this.btQuitter.Click += new System.EventHandler(this.btQuitter_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 706);
            this.Controls.Add(this.btQuitter);
            this.Controls.Add(this.tbJoueurScore);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.btValider);
            this.Controls.Add(this.tbLogin);
            this.Name = "Form1";
            this.Text = "Connexion";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Button btValider;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.TextBox tbJoueurScore;
        private System.Windows.Forms.Button btQuitter;
    }
}

