﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppliTP
{
    public partial class FormMenu : Form
    {
        public FormMenu()
        {
            InitializeComponent();
        }

        private void FormMenu_Load(object sender, EventArgs e)
        {
            fProfil = new FormProfil();
            fLire = new FormLire();
            fCompter = new FormCompter();
        }

        private void btProfil_Click(object sender, EventArgs e)
        {
            this.Hide();
            DialogResult voirProfil = fProfil.ShowDialog();
            if (voirProfil == DialogResult.Cancel)
            {
            }
            this.Show();
        }

        private void btLire_Click(object sender, EventArgs e)
        {
            this.Hide();
            DialogResult apprendreLire = fLire.ShowDialog();
            if (apprendreLire == DialogResult.Cancel)
            {
            }
            this.Show();

        }

        private void btCompter_Click(object sender, EventArgs e)
        {
            this.Hide();
            DialogResult apprendreCompter = fCompter.ShowDialog();
            if (apprendreCompter == DialogResult.Cancel)
            {
            }
            this.Show();
        }

        private void btDeco_Click(object sender, EventArgs e)
        {   
        }
    }
}
