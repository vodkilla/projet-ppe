﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppliTP
{
    public partial class FormCompter : Form
    {
        public FormCompter()
        {
            InitializeComponent();
        }
        private void FormCompter_Load(object sender, EventArgs e)
        {
            //Affiche l'opérateur, + par défaut
            lbOp.Text = "+";
            // Affiche le score
            lbScore.Text = Convert.ToString(Outils.GetScoreCompter());
        }
        private void tbRep_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
                e.Handled = true;
        }
        private void btValider_Click(object sender, EventArgs e)
        {
            
            Outils.IncrementerScoreCompter();
            Outils.SetFichier();
            lbScore.Text = Convert.ToString(Outils.GetScoreCompter());
        }
        private void rbMoins_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMoins.Checked == true)
            {
                lbOp.Text = "-";
            }
            else
                lbOp.Text = "+";
        }
    }
}
