﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AppliTP
{
    class Outils
    {
        private static string dossier = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\AppliTp";
        private static string fichier = Convert.ToString(dossier + @"\log.txt");
        private static string[] lireFichier;
        private static string[,] variableFichier;
        private static int scoreLire, scoreCompter, animal;
        private static string prenom, nomImage;
        //!\ Ce tableau correspond aux noms des images de listimage1 
        private static string[] animaux = { "chat", "cheval", "chien", "dauphin", "girafe", "lapin", "lion", "oiseaux", "ours", "poule", "tortue", "vache" };
        private static Random generateur = new Random();

        public Outils()
        {
        }
        // A FAIRE
        // Classement score
        
        /////FICHIER ///////

        //Verification que le fichier et dossier existe sinon on les crées
        public static void verifChemin()
        {
            if (Directory.Exists(dossier))
            {
                if (File.Exists(fichier))
                {
                }
                else
                {
                    File.WriteAllText(fichier, "");
                }
            }
            else
            {
                Directory.CreateDirectory(dossier);
                File.WriteAllText(fichier, "");
            }

        }
        //recupere le fichier dans un tableau
        public static string[,] GetFichier()
        {
            verifChemin();
            lireFichier = System.IO.File.ReadAllLines(fichier);
            variableFichier = new string[lireFichier.Length, 3];
            int numLigne = 0;
            foreach (string line in lireFichier)
            {
                string[] separer;
                separer = line.Split(',');
                for (int i = 0; i < 3; i++)
                {
                    variableFichier[numLigne, i] += (separer[i]);
                }
                numLigne += 1;
            }
            return variableFichier;
        }
        //Reecrire le fichier
        public static void SetFichier()
        {
            string txt = "";
            for (int ligne = 0; ligne < lireFichier.Length; ligne++)
            {
                txt += (variableFichier[ligne, 0].ToString() + "," + variableFichier[ligne, 1].ToString() + "," + variableFichier[ligne, 2].ToString() + Environment.NewLine);
            }
            System.IO.File.WriteAllText(fichier, txt);

        }

        ///CONNEXION ET AFFICHAGE ////

        // On recupere le prenom et score
        public static bool Connexion(string loginEntre)
        {
            bool valide = false;
            for (int ligne = 0; ligne < lireFichier.Length; ligne++)
            {
                for (int col = 0; col < 3; col++)
                {
                    if (loginEntre.ToUpper() == variableFichier[ligne, col])
                    {
                        valide = true;
                        prenom = variableFichier[ligne, col];
                        scoreLire = Convert.ToInt16(variableFichier[ligne, col + 1]);
                        scoreCompter = Convert.ToInt16(variableFichier[ligne, col + 2]);
                    }
                }
            }
            return valide;
        }
        //afficher tableau
        public static string Afficher()
        {
            string joueurScore = ("Prénom \t Score lire \t Score compter \t Score Total")+Environment.NewLine;
            int scoreTotal = 0;
            for (int ligne = 0; ligne < lireFichier.Length; ligne++)
            {
                for (int col = 0; col < 3; col++)
                {
                    joueurScore += variableFichier[ligne, col] + "\t\t";
                }
                scoreTotal = Convert.ToInt16(variableFichier[ligne, 1]) + Convert.ToInt16(variableFichier[ligne, 2]);
                joueurScore += scoreTotal+Environment.NewLine;
            }
            return joueurScore;
        }
        
        //////UTILISATEUR///////

        //Verifier si un utilisateur existe
        public static bool verifDoublon(string pseudo)
        {
            bool valide = true;
            for (int ligne = 0; ligne < lireFichier.Length; ligne++)
            {
                if (pseudo.ToUpper() == variableFichier[ligne, 0])
                {
                    valide = false;
                }
            }
            return valide;
        }
        //Ajouter un utilisateur
        // On relis le fichier pour l'afficher
        public static void AjouterUtilisateur(string pseudo)
        {
            using (StreamWriter sw = File.AppendText(fichier))
            {
                sw.WriteLine(pseudo.ToUpper() + ",0,0");
            }
            GetFichier();
        }

        /////////MODULE LIRE /////////
        //Bonne reponse module lire
        public static int IncrementerScoreLire()
        {
            scoreLire += 1;
            for (int ligne = 0; ligne < lireFichier.Length; ligne++)
            {
                for (int col = 0; col < 3; col++)
                {
                    if (prenom == variableFichier[ligne, col])
                    {
                        prenom = variableFichier[ligne, col];
                        variableFichier[ligne, col + 1] = Convert.ToString(scoreLire);
                        scoreCompter = Convert.ToInt16(variableFichier[ligne, col + 2]);
                    }
                }
            }
            return scoreLire;
        }

        // genere le nom de l'image a afficher
        public static void SetNomImage()
        {
            animal = generateur.Next(0, animaux.Length);
            nomImage = animaux[animal];
        }
        public static string GetNomImage()
        {
            return nomImage;
        }
        ///// GENERATION DES RADIOBOUTON
        //texte des radio bouton du module lire
        // Different de l'image
        public static string GenTexteRbDiff1(string nomImage)
        {
            animal = generateur.Next(0, animaux.Length);
            while(animaux[animal] == nomImage)
            {
            animal = generateur.Next(0, animaux.Length);
            }
            return animaux[animal];
        }
        public static string GenTexteRbDiff2(string nomImage, string rb1)
        {
            animal = generateur.Next(0, animaux.Length);
            while (animaux[animal] == nomImage || animaux[animal] == rb1)
            {
                animal = generateur.Next(0, animaux.Length);
            }
            return animaux[animal];
        }
        /////////MODULE COMPTER /////////

        //Bonne reponse module compter
        public static int IncrementerScoreCompter()
        {
            scoreCompter += 1;
            for (int ligne = 0; ligne < lireFichier.Length; ligne++)
            {
                for (int col = 0; col < 3; col++)
                {
                    if (prenom == variableFichier[ligne, col])
                    {
                        prenom = variableFichier[ligne, col];
                        variableFichier[ligne, col + 2] = Convert.ToString(scoreCompter);
                        scoreLire = Convert.ToInt16(variableFichier[ligne, col + 1]);
                    }
                }
            }
            return scoreLire;
        }

        public static string GetPrenom()
        {
            return prenom;
        }
        public static int GetScoreLire()
        {
            return scoreLire;
        }
        public static int GetScoreCompter()
        {
            return scoreCompter;
        }
    }
}
