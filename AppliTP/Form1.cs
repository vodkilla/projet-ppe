﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppliTP
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            fMenu = new FormMenu();
            fAjout = new FormAjout();
            //Recuperation du contenu du fichier texte
            Outils.GetFichier();
            //Affichage des scores et pseudo
            tbJoueurScore.Text = Outils.Afficher();
        }

        private void btValider_Click(object sender, EventArgs e)
        {
            //Il faut un login valide pour passer a la fenetre principale
            if (Outils.Connexion(tbLogin.Text) == true)
            {
                this.Hide();
                DialogResult session = fMenu.ShowDialog();
                if (session == DialogResult.Cancel)
                {
                    tbLogin.Text = "";
                    tbJoueurScore.Text = "";
                    tbJoueurScore.Text = Outils.Afficher();
                }
                this.Show();
                
            }
            // Indication que le login n'est pas reconnu et proposition d'en créer un
            else
            {
                MessageBox.Show("Clique sur le bouton ajouter pour créer ton premier compte \n ou en créer un nouveau", "Problème de mémoire?");
            }
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            this.Hide();
            DialogResult ajoutPseudo = fAjout.ShowDialog();
            if (ajoutPseudo == DialogResult.OK)
            {
                tbLogin.Text = "";
                tbJoueurScore.Text = "";
                tbJoueurScore.Text = Outils.Afficher();
            }
            this.Show();
        }

        private void btQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
