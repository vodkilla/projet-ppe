﻿namespace AppliTP
{
    partial class FormMenu
    {
        // Déclaration des fenêtre
        FormProfil fProfil;
        FormLire fLire;
        FormCompter fCompter;
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btLire = new System.Windows.Forms.Button();
            this.btCompter = new System.Windows.Forms.Button();
            this.btProfil = new System.Windows.Forms.Button();
            this.btDeco = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(329, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // btLire
            // 
            this.btLire.Location = new System.Drawing.Point(70, 159);
            this.btLire.Name = "btLire";
            this.btLire.Size = new System.Drawing.Size(187, 570);
            this.btLire.TabIndex = 1;
            this.btLire.Text = "Lire";
            this.btLire.UseVisualStyleBackColor = true;
            this.btLire.Click += new System.EventHandler(this.btLire_Click);
            // 
            // btCompter
            // 
            this.btCompter.Location = new System.Drawing.Point(392, 118);
            this.btCompter.Name = "btCompter";
            this.btCompter.Size = new System.Drawing.Size(187, 570);
            this.btCompter.TabIndex = 2;
            this.btCompter.Text = "Compter";
            this.btCompter.UseVisualStyleBackColor = true;
            this.btCompter.Click += new System.EventHandler(this.btCompter_Click);
            // 
            // btProfil
            // 
            this.btProfil.Location = new System.Drawing.Point(678, 140);
            this.btProfil.Name = "btProfil";
            this.btProfil.Size = new System.Drawing.Size(187, 570);
            this.btProfil.TabIndex = 3;
            this.btProfil.Text = "Progression";
            this.btProfil.UseVisualStyleBackColor = true;
            this.btProfil.Click += new System.EventHandler(this.btProfil_Click);
            // 
            // btDeco
            // 
            this.btDeco.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btDeco.Location = new System.Drawing.Point(375, 759);
            this.btDeco.Name = "btDeco";
            this.btDeco.Size = new System.Drawing.Size(187, 35);
            this.btDeco.TabIndex = 4;
            this.btDeco.Text = "Déco";
            this.btDeco.UseVisualStyleBackColor = true;
            this.btDeco.Click += new System.EventHandler(this.btDeco_Click);
            // 
            // FormMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 806);
            this.Controls.Add(this.btDeco);
            this.Controls.Add(this.btProfil);
            this.Controls.Add(this.btCompter);
            this.Controls.Add(this.btLire);
            this.Controls.Add(this.label1);
            this.Name = "FormMenu";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.FormMenu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btLire;
        private System.Windows.Forms.Button btCompter;
        private System.Windows.Forms.Button btProfil;
        private System.Windows.Forms.Button btDeco;
    }
}