﻿namespace AppliTP
{
    partial class FormAjout
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.tbAjout = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btAnnuler
            // 
            this.btAnnuler.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btAnnuler.Location = new System.Drawing.Point(197, 652);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(148, 54);
            this.btAnnuler.TabIndex = 0;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(678, 652);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(148, 54);
            this.btAjouter.TabIndex = 1;
            this.btAjouter.Text = "Ajouter";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // tbAjout
            // 
            this.tbAjout.Location = new System.Drawing.Point(237, 169);
            this.tbAjout.Name = "tbAjout";
            this.tbAjout.Size = new System.Drawing.Size(589, 31);
            this.tbAjout.TabIndex = 2;
            this.tbAjout.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAjout_TextChanged);
            // 
            // FormAjout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 746);
            this.Controls.Add(this.tbAjout);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.btAnnuler);
            this.Name = "FormAjout";
            this.Text = "Ajouter un joueur";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.TextBox tbAjout;
    }
}