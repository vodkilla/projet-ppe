﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppliTP
{
    public partial class FormProfil : Form
    {
        public FormProfil()
        {
            InitializeComponent();
        }

        private void FormProfil_Load(object sender, EventArgs e)
        {
            lbPrenom.Text = Outils.GetPrenom();
            lbLire.Text =Convert.ToString(Outils.GetScoreLire());
            lbCompter.Text = Convert.ToString(Outils.GetScoreCompter());
            pgLire.Value = Outils.GetScoreLire();
            pgCompter.Value = Outils.GetScoreCompter();
            lbTotal.Text = Convert.ToString(Outils.GetScoreCompter() + Outils.GetScoreLire());
        }
    }
}
