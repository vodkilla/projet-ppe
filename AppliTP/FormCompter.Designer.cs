﻿namespace AppliTP
{
    partial class FormCompter
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btValider = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbChiffre1 = new System.Windows.Forms.Label();
            this.lbChiffre2 = new System.Windows.Forms.Label();
            this.lbOp = new System.Windows.Forms.Label();
            this.tbRep = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbPlus = new System.Windows.Forms.RadioButton();
            this.rbMoins = new System.Windows.Forms.RadioButton();
            this.lbScore = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btValider
            // 
            this.btValider.Location = new System.Drawing.Point(622, 612);
            this.btValider.Name = "btValider";
            this.btValider.Size = new System.Drawing.Size(196, 88);
            this.btValider.TabIndex = 3;
            this.btValider.Text = "Valider";
            this.btValider.UseVisualStyleBackColor = true;
            this.btValider.Click += new System.EventHandler(this.btValider_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(159, 612);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(196, 88);
            this.btCancel.TabIndex = 2;
            this.btCancel.Text = "Retour";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(303, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(429, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Vous devez entrer le resultat de l\'opération.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(456, 388);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 26);
            this.label2.TabIndex = 5;
            this.label2.Text = "=";
            // 
            // lbChiffre1
            // 
            this.lbChiffre1.AutoSize = true;
            this.lbChiffre1.Location = new System.Drawing.Point(229, 269);
            this.lbChiffre1.Name = "lbChiffre1";
            this.lbChiffre1.Size = new System.Drawing.Size(83, 26);
            this.lbChiffre1.TabIndex = 6;
            this.lbChiffre1.Text = "chiffre1";
            // 
            // lbChiffre2
            // 
            this.lbChiffre2.AutoSize = true;
            this.lbChiffre2.Location = new System.Drawing.Point(581, 281);
            this.lbChiffre2.Name = "lbChiffre2";
            this.lbChiffre2.Size = new System.Drawing.Size(83, 26);
            this.lbChiffre2.TabIndex = 7;
            this.lbChiffre2.Text = "chiffre2";
            // 
            // lbOp
            // 
            this.lbOp.AutoSize = true;
            this.lbOp.Location = new System.Drawing.Point(456, 269);
            this.lbOp.Name = "lbOp";
            this.lbOp.Size = new System.Drawing.Size(36, 26);
            this.lbOp.TabIndex = 8;
            this.lbOp.Text = "op";
            // 
            // tbRep
            // 
            this.tbRep.Location = new System.Drawing.Point(420, 459);
            this.tbRep.Name = "tbRep";
            this.tbRep.Size = new System.Drawing.Size(100, 31);
            this.tbRep.TabIndex = 9;
            this.tbRep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRep_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbMoins);
            this.groupBox1.Controls.Add(this.rbPlus);
            this.groupBox1.Location = new System.Drawing.Point(823, 256);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 304);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opérateur";
            // 
            // rbPlus
            // 
            this.rbPlus.AutoSize = true;
            this.rbPlus.Location = new System.Drawing.Point(49, 72);
            this.rbPlus.Name = "rbPlus";
            this.rbPlus.Size = new System.Drawing.Size(50, 30);
            this.rbPlus.TabIndex = 0;
            this.rbPlus.TabStop = true;
            this.rbPlus.Text = "+";
            this.rbPlus.UseVisualStyleBackColor = true;
            // 
            // rbMoins
            // 
            this.rbMoins.AutoSize = true;
            this.rbMoins.Location = new System.Drawing.Point(49, 164);
            this.rbMoins.Name = "rbMoins";
            this.rbMoins.Size = new System.Drawing.Size(44, 30);
            this.rbMoins.TabIndex = 1;
            this.rbMoins.TabStop = true;
            this.rbMoins.Text = "-";
            this.rbMoins.UseVisualStyleBackColor = true;
            this.rbMoins.CheckedChanged += new System.EventHandler(this.rbMoins_CheckedChanged);
            // 
            // lbScore
            // 
            this.lbScore.AutoSize = true;
            this.lbScore.Location = new System.Drawing.Point(718, 487);
            this.lbScore.Name = "lbScore";
            this.lbScore.Size = new System.Drawing.Size(0, 26);
            this.lbScore.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(598, 487);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 26);
            this.label3.TabIndex = 12;
            this.label3.Text = "Score:";
            // 
            // FormCompter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1164, 800);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbScore);
            this.Controls.Add(this.tbRep);
            this.Controls.Add(this.lbOp);
            this.Controls.Add(this.lbChiffre2);
            this.Controls.Add(this.lbChiffre1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btValider);
            this.Controls.Add(this.btCancel);
            this.Name = "FormCompter";
            this.Text = "Compter";
            this.Load += new System.EventHandler(this.FormCompter_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btValider;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbChiffre1;
        private System.Windows.Forms.Label lbChiffre2;
        private System.Windows.Forms.Label lbOp;
        private System.Windows.Forms.TextBox tbRep;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbMoins;
        private System.Windows.Forms.RadioButton rbPlus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbScore;
    }
}