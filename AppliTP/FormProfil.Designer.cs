﻿namespace AppliTP
{
    partial class FormProfil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lbPrenom = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pgLire = new System.Windows.Forms.ProgressBar();
            this.pgCompter = new System.Windows.Forms.ProgressBar();
            this.btMenu = new System.Windows.Forms.Button();
            this.lbLire = new System.Windows.Forms.Label();
            this.lbCompter = new System.Windows.Forms.Label();
            this.lbTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prénom:";
            // 
            // lbPrenom
            // 
            this.lbPrenom.AutoSize = true;
            this.lbPrenom.Location = new System.Drawing.Point(185, 99);
            this.lbPrenom.Name = "lbPrenom";
            this.lbPrenom.Size = new System.Drawing.Size(24, 26);
            this.lbPrenom.TabIndex = 1;
            this.lbPrenom.Text = "..";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 266);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lire:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 406);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 26);
            this.label3.TabIndex = 3;
            this.label3.Text = "Compter";
            // 
            // pgLire
            // 
            this.pgLire.Location = new System.Drawing.Point(374, 266);
            this.pgLire.Name = "pgLire";
            this.pgLire.Size = new System.Drawing.Size(436, 23);
            this.pgLire.TabIndex = 4;
            // 
            // pgCompter
            // 
            this.pgCompter.Location = new System.Drawing.Point(350, 406);
            this.pgCompter.Name = "pgCompter";
            this.pgCompter.Size = new System.Drawing.Size(429, 23);
            this.pgCompter.TabIndex = 5;
            // 
            // btMenu
            // 
            this.btMenu.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btMenu.Location = new System.Drawing.Point(505, 647);
            this.btMenu.Name = "btMenu";
            this.btMenu.Size = new System.Drawing.Size(75, 90);
            this.btMenu.TabIndex = 6;
            this.btMenu.Text = "Retour";
            this.btMenu.UseVisualStyleBackColor = true;
            // 
            // lbLire
            // 
            this.lbLire.AutoSize = true;
            this.lbLire.Location = new System.Drawing.Point(887, 268);
            this.lbLire.Name = "lbLire";
            this.lbLire.Size = new System.Drawing.Size(36, 26);
            this.lbLire.TabIndex = 8;
            this.lbLire.Text = " ...";
            // 
            // lbCompter
            // 
            this.lbCompter.AutoSize = true;
            this.lbCompter.Location = new System.Drawing.Point(887, 370);
            this.lbCompter.Name = "lbCompter";
            this.lbCompter.Size = new System.Drawing.Size(36, 26);
            this.lbCompter.TabIndex = 9;
            this.lbCompter.Text = " ...";
            // 
            // lbTotal
            // 
            this.lbTotal.AutoSize = true;
            this.lbTotal.Location = new System.Drawing.Point(922, 454);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(36, 26);
            this.lbTotal.TabIndex = 10;
            this.lbTotal.Text = " ...";
            // 
            // FormProfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 762);
            this.Controls.Add(this.lbTotal);
            this.Controls.Add(this.lbCompter);
            this.Controls.Add(this.lbLire);
            this.Controls.Add(this.btMenu);
            this.Controls.Add(this.pgCompter);
            this.Controls.Add(this.pgLire);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbPrenom);
            this.Controls.Add(this.label1);
            this.Name = "FormProfil";
            this.Text = "Profil";
            this.Load += new System.EventHandler(this.FormProfil_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbPrenom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar pgLire;
        private System.Windows.Forms.ProgressBar pgCompter;
        private System.Windows.Forms.Button btMenu;
        private System.Windows.Forms.Label lbLire;
        private System.Windows.Forms.Label lbCompter;
        private System.Windows.Forms.Label lbTotal;
    }
}