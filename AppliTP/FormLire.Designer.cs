﻿namespace AppliTP
{
    partial class FormLire
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLire));
            this.btCancel = new System.Windows.Forms.Button();
            this.btValider = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.rbChoix1 = new System.Windows.Forms.RadioButton();
            this.rbChoix2 = new System.Windows.Forms.RadioButton();
            this.rbChoix3 = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lbScore = new System.Windows.Forms.Label();
            this.lbAttention = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbReponse = new System.Windows.Forms.Label();
            this.lbChoix = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(237, 721);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(196, 88);
            this.btCancel.TabIndex = 0;
            this.btCancel.Text = "Retour";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // btValider
            // 
            this.btValider.Location = new System.Drawing.Point(726, 721);
            this.btValider.Name = "btValider";
            this.btValider.Size = new System.Drawing.Size(196, 88);
            this.btValider.TabIndex = 1;
            this.btValider.Text = "Valider";
            this.btValider.UseVisualStyleBackColor = true;
            this.btValider.Click += new System.EventHandler(this.btValider_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(328, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(535, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vous devez cocher le bouton correspondant a l\'image.";
            // 
            // rbChoix1
            // 
            this.rbChoix1.AutoSize = true;
            this.rbChoix1.Location = new System.Drawing.Point(69, 66);
            this.rbChoix1.Name = "rbChoix1";
            this.rbChoix1.Size = new System.Drawing.Size(160, 30);
            this.rbChoix1.TabIndex = 3;
            this.rbChoix1.TabStop = true;
            this.rbChoix1.Text = "radioButton1";
            this.rbChoix1.UseVisualStyleBackColor = true;
            // 
            // rbChoix2
            // 
            this.rbChoix2.AutoSize = true;
            this.rbChoix2.Location = new System.Drawing.Point(69, 125);
            this.rbChoix2.Name = "rbChoix2";
            this.rbChoix2.Size = new System.Drawing.Size(160, 30);
            this.rbChoix2.TabIndex = 4;
            this.rbChoix2.TabStop = true;
            this.rbChoix2.Text = "radioButton2";
            this.rbChoix2.UseVisualStyleBackColor = true;
            // 
            // rbChoix3
            // 
            this.rbChoix3.AutoSize = true;
            this.rbChoix3.Location = new System.Drawing.Point(69, 200);
            this.rbChoix3.Name = "rbChoix3";
            this.rbChoix3.Size = new System.Drawing.Size(160, 30);
            this.rbChoix3.TabIndex = 5;
            this.rbChoix3.TabStop = true;
            this.rbChoix3.Text = "radioButton3";
            this.rbChoix3.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(67, 118);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(559, 497);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.WaitOnLoad = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "canard");
            this.imageList1.Images.SetKeyName(1, "chat");
            this.imageList1.Images.SetKeyName(2, "cheval");
            this.imageList1.Images.SetKeyName(3, "chien");
            this.imageList1.Images.SetKeyName(4, "dauphin");
            this.imageList1.Images.SetKeyName(5, "girafe");
            this.imageList1.Images.SetKeyName(6, "lapin");
            this.imageList1.Images.SetKeyName(7, "lion");
            this.imageList1.Images.SetKeyName(8, "oiseaux");
            this.imageList1.Images.SetKeyName(9, "ours");
            this.imageList1.Images.SetKeyName(10, "poule");
            this.imageList1.Images.SetKeyName(11, "tortue");
            this.imageList1.Images.SetKeyName(12, "vache");
            // 
            // lbScore
            // 
            this.lbScore.AutoSize = true;
            this.lbScore.Location = new System.Drawing.Point(158, 319);
            this.lbScore.Name = "lbScore";
            this.lbScore.Size = new System.Drawing.Size(0, 26);
            this.lbScore.TabIndex = 9;
            // 
            // lbAttention
            // 
            this.lbAttention.AutoSize = true;
            this.lbAttention.Location = new System.Drawing.Point(555, 662);
            this.lbAttention.Name = "lbAttention";
            this.lbAttention.Size = new System.Drawing.Size(0, 26);
            this.lbAttention.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rbChoix3);
            this.groupBox1.Controls.Add(this.lbScore);
            this.groupBox1.Controls.Add(this.rbChoix2);
            this.groupBox1.Controls.Add(this.rbChoix1);
            this.groupBox1.Location = new System.Drawing.Point(794, 197);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(332, 383);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nom de l\'animal";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 319);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 26);
            this.label2.TabIndex = 10;
            this.label2.Text = "Ton score:";
            // 
            // lbReponse
            // 
            this.lbReponse.AutoSize = true;
            this.lbReponse.Location = new System.Drawing.Point(793, 99);
            this.lbReponse.Name = "lbReponse";
            this.lbReponse.Size = new System.Drawing.Size(0, 26);
            this.lbReponse.TabIndex = 12;
            // 
            // lbChoix
            // 
            this.lbChoix.AutoSize = true;
            this.lbChoix.Location = new System.Drawing.Point(793, 143);
            this.lbChoix.Name = "lbChoix";
            this.lbChoix.Size = new System.Drawing.Size(0, 26);
            this.lbChoix.TabIndex = 13;
            // 
            // FormLire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 849);
            this.Controls.Add(this.lbChoix);
            this.Controls.Add(this.lbReponse);
            this.Controls.Add(this.lbAttention);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btValider);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormLire";
            this.Text = "Lecture";
            this.Load += new System.EventHandler(this.FormLire_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btValider;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbChoix1;
        private System.Windows.Forms.RadioButton rbChoix2;
        private System.Windows.Forms.RadioButton rbChoix3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label lbScore;
        private System.Windows.Forms.Label lbAttention;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbReponse;
        private System.Windows.Forms.Label lbChoix;
    }
}